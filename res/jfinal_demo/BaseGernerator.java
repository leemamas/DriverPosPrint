package jfinal_demo;

import javax.sql.DataSource;

import org.sqlite.SQLiteDataSource;

import com.demo.common.config.DemoConfig;
import com.jfinal.kit.PathKit;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.dialect.Sqlite3Dialect;
import com.jfinal.plugin.activerecord.generator.Generator;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.jfinal.plugin.druid.DruidPlugin;

public class BaseGernerator {

	public static DataSource getDataSource() {
		PropKit.use("config.cfg");
//		C3p0Plugin c3p0Plugin = DemoConfig.createC3p0Plugin();
//		c3p0Plugin.start();
//		return c3p0Plugin.getDataSource();
		DruidPlugin dp = new DruidPlugin(PropKit.get("jdbcUrl"), PropKit.get("name"), PropKit.get("password"));
		dp.start();
		return dp.getDataSource();
	}
	
	public static DataSource getDataSources() {
		String dbPath = PathKit.getWebRootPath() + "/demo.db";
		System.out.println(dbPath);
		String dbUrl = "jdbc:sqlite:"+dbPath;
		C3p0Plugin c3p0Plugin = new C3p0Plugin(dbUrl, "", "");
		c3p0Plugin.setDriverClass("org.sqlite.JDBC");
		c3p0Plugin.start();

		return c3p0Plugin.getDataSource();
	}
	
	public static void main(String[] args) {
		// base model 所使用的包名
		String baseModelPackageName = "com.demo.common.model.base";
		// base model 文件保存路径
		String baseModelOutputDir = PathKit.getWebRootPath() + "/../src/com/demo/common/model/base";
		
		// model 所使用的包名 (MappingKit 默认使用的包名)
		String modelPackageName = "com.demo.common.model";
		// model 文件保存路径 (MappingKit 与 DataDictionary 文件默认保存路径)
		String modelOutputDir = baseModelOutputDir + "/..";
		
		// 创建生成器
		Generator gernerator = new Generator(getDataSources(), baseModelPackageName, baseModelOutputDir, modelPackageName, modelOutputDir);
		// 添加不需要生成的表名
		gernerator.addExcludedTable("adv");
		// 设置是否在 Model 中生成 dao 对象
		gernerator.setGenerateDaoInModel(true);
		// 设置是否生成字典文件
		gernerator.setGenerateDataDictionary(true);
		// 设置需要被移除的表名前缀用于生成modelName。例如表名 "osc_user"，移除前缀 "osc_"后生成的model名为 "User"而非 OscUser
		gernerator.setRemovedTableNamePrefixes("t_");
		// 生成
		gernerator.generate();
	}
}
