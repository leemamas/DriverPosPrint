package com.demo.test;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.junit.BeforeClass;
import org.junit.Test;

import com.demo.common.model._MappingKit;
import com.jfinal.kit.PathKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.dialect.Sqlite3Dialect;
import com.jfinal.plugin.c3p0.C3p0Plugin;

public class BaseTest {

	@BeforeClass
	public static void before(){
		String dbPath = PathKit.getWebRootPath() + "/demo.db";
		System.out.println(dbPath);
		String dbUrl = "jdbc:sqlite:"+dbPath;
		C3p0Plugin c3p0Plugin = new C3p0Plugin(dbUrl, "", "");
		c3p0Plugin.setDriverClass("org.sqlite.JDBC");
		c3p0Plugin.start();

		ActiveRecordPlugin arp = new ActiveRecordPlugin(c3p0Plugin);
		arp.setDialect(new Sqlite3Dialect());
		_MappingKit.mapping(arp);
		arp.start();
		
	}
	
	@Test
	public void test(){
		String str = "点菜成功！1001 无此菜 本次点菜3/3份 2成功，1失败合计XX元祝您用餐愉快a";
		try {
			System.out.println(str.getBytes("GBK").length);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
	}
	
	@SuppressWarnings("resource")
	public static String readTxt(String filePathAndName, String encoding)
			throws IOException {
		encoding = encoding.trim();
		StringBuffer str = new StringBuffer("");
		String st = "";
		try {
			FileInputStream fs = new FileInputStream(filePathAndName);
			InputStreamReader isr;
			if (encoding.equals("")) {
				isr = new InputStreamReader(fs);
			} else {
				isr = new InputStreamReader(fs, encoding);
			}
			BufferedReader br = new BufferedReader(isr);
			try {
				String data = "";
				while ((data = br.readLine()) != null) {
					str.append(data);
				}
			} catch (Exception e) {
				str.append(e.toString());
			}
			st = str.toString();
			if (st != null && st.length() > 1)
				st = st.substring(0, st.length());
		} catch (IOException es) {
			st = "";
		}
		return st;
	}
}
